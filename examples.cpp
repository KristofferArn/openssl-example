#include "crypto.hpp"
#include <iostream>

using namespace std;

int main() {
    cout << "SHA-1 with 1 iteration" << endl;
    cout << Crypto::hex(Crypto::sha1("Test")) << endl << endl;

    cout << "SHA-1 with two iterations" << endl;
    cout << Crypto::hex(Crypto::sha1(Crypto::sha1("Test"))) << endl;

    cout << "The derived key from the PBKDF2 algorithm" << endl;
    cout << Crypto::hex(Crypto::pbkdf2("Password", "Salt")) << endl;

    cout << "MD5 hash:" << endl;
    cout << Crypto::hex(Crypto::md5("Test")) << endl;

    cout << "SHA256 hash:" << endl;
    cout << Crypto::hex(Crypto::sha256("Test")) << endl;

    cout << "SHA512 hash:" << endl;
    cout << Crypto::hex(Crypto::sha512("Test")) << endl;

    //Password cracking
//    cout << endl;
//    std::string hash = Crypto::hex(Crypto::pbkdf2("QwE", "Saltet til Ola", 2048, 160));
//    cout << hash << endl;
//    if (hash == "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6") cout << "Password cracked!" << endl;
//    else cout << "Not cracked..." << endl;

    std::string alf = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    std::string password;
    std::string hash;
    for (size_t x = 0;  x < alf.size() ; ++x) {
        for (size_t y = 0;  y < alf.size() ; ++y) {
            for (size_t z = 0;  z < alf.size() ; ++z) {
                password = alf[x];
                password += alf[y];
                password += alf[z];
                hash = Crypto::hex(Crypto::pbkdf2(password, "Saltet til Ola", 2048, 160));
                cout << password << endl;
                if (hash == "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6") {
                    cout << "Cracked: " + password << endl;
                    return 0;
                }
            }
        }
    }
    return 0;

//Password QwE
}
